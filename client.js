import React from 'react'
import reduxThunk from 'redux-thunk'
import { hydrate } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import {
  BrowserRouter,
  browserHistory
} from 'react-router-dom'

import rootReducers from './client/reducers/_index'
import App from './client/app'

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore)
let store = createStoreWithMiddleware(rootReducers)

let unsub = store.subscribe(() => {
  console.log(store.getState())
})

hydrate(
  <Provider store={store}>
    <BrowserRouter history={browserHistory}>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
)
