//types

export const inc = 'increase'

//actions

export function counter(num) {
  return { type: inc, num }
}
