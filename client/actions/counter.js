export const increase = 'increase'
function increaseCount(data) {
  return {
    type: increase, data
  }
}

export const decrease = 'decrease'
function decreaseCount(data) {
  return {
    type: decrease, data
  }
}
