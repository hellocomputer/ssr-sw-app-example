import axios from 'axios'

const url = 'https://react-ssr-app.herokuapp.com'

export function userWebpush(subscription) {
  return function(dispatch) {
    axios.post(url + '/api/webpush', {
      data: subscription
    })
    .then(res => {
      return
    })
    .catch(err => {
      return
    })
  }
}

export function userSubscribe(subscription, active) {
  return function(dispatch) {
    axios.post(url + '/api/subscribe', {
      subscription: subscription,
      active: active
    })
    .then(res => {
      return
    })
    .catch(err => {
      return
    })
  }
}

export const logout = 'logout'
export function userLogout() {
  return function(dispatch) {
    axios.post(url + '/api/logout', {})
    .then(res => {
      dispatch({
        type: logout,
        isAuth: res.data.isAuth,
        msg: res.data.message
      })
    })
    .catch((err) => {
      console.log(err)
    })
  }
}

export const verify = 'verify'
export function userVerify() {
  return function(dispatch) {
    axios.post(url + '/api/verify', {})
    .then(res => {
      dispatch({
        type: verify,
        isAuth: res.data.isAuth,
        msg: res.data.message,
        isFetching: false,
        apn: res.data.apn
      })
    })
    .catch((err) => {
      dispatch({
        type: verify,
        isAuth: true,
        msg: "Welcome to offline mode",
        isFetching: false,
        apn: null
      })
    })
  }
}

export const login = 'login'
export function userLogin(name, pass) {
  return function(dispatch) {
    axios.post(url + '/api/login', {
      username: name,
      password: pass,
    })
    .then(res => {
      dispatch({
        type: login,
        isAuth: res.data.isAuth,
        msg: res.data.message,
        apn: res.data.apn
      })
    })
    .catch((err) => {
      console.log(err)
    })
  }
}

export const register = 'register'
export function userReg(name, pass) {
  return function(dispatch) {
    axios.post(url + '/api/register', {
      username: name,
      password: pass,
    })
    .then(res => {
      dispatch({
        type: register,
        isAuth: res.data.isAuth,
        msg: res.data.message
      })
    })
    .catch((err) => {
      console.log(err)
    })
  }
}
