import axios from 'axios'

const url = 'http://localhost:3001'

export const createBoxes = 'createBoxes'
export function boxes() {
  return function(dispatch) {
    axios.get(url + '/api/box')
    .then(res => {
      dispatch({
        type: createBoxes,
        payload: res.data
      });
    })
    .catch((error) => {
      console.log(error);
    })
  }
}
