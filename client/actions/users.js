import axios from 'axios'

const url = 'https://react-ssr-app.herokuapp.com'

export const get = 'get'
export function Endpoints() {
  return (dispatch) => {
    axios.get(url + '/api/users')
    .then(res => {
      dispatch({
        type: get,
        users: res.data.userlist
      })
    })
    .catch(err => {
      console.log(err)
    })
  }
}

export const push = 'push'
export function Notifications(target, title, body) {
  return () => {
    axios.post(url + '/api/push', {
      target: target,
      title, title,
      body: body
    })
    .then(res => {
      return
    })
    .catch(err => {
      console.log(err)
    })
  }
}
