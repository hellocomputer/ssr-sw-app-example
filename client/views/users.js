import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Endpoints, Notifications } from '../actions/users'

class Users extends Component {
  constructor() {
    super()

    this.state = {
      target: '',
      title: '',
      body: '',
      current: null
    }
  }

  toggleStyles = (element) => {
    return {
      margin: '0 auto',
      backgroundColor: this.state.current == element ? "#C7E78B" : "#fff",
      width: '300px',
      border: '2px solid black',
      borderRadius: '2px'
    }
  }

  selectStyle = () => {
    return {
      width: '25px',
      height: '25px',
      display: 'inline-block',
      left: '25px',
      verticalAlign: 'middle',
      position: 'relative',
      border: '1px solid black'
    }
  }

  handleSelect = (event) => {
    this.setState({
      target: event.target.attributes["data-sub"].value,
      current: event.target.attributes["data-selected"].value
    })
  }

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  handleClick = () => {
    const { target, title, body, current } = this.state
    const { Push } = this.props

    if (current) {
      Push(target, title, body)
    } else {
      console.log("Please select a user you would like to send a push notification to")
    }
  }

  componentWillMount() {
    this.props.Get()
  }

  render () {
    const { Users } = this.props
    console.log(Users)
    return (
      <div className="users-page">
        <div className="users-container">
          <div className="user-wrap">
            {
              Users.map((item, index) => {
                let user = {
                  "data-sub": `${item.subscription}`,
                  "data-selected" : `${item.name}`
                }
                return (
                  <div key={index} className="push-sub"  style={this.toggleStyles(item.name)}>
                    <p style={{display: 'inline-block', margin: '10px'}}>Name: {item.name}</p>
                    <p style={{display: 'inline-block'}}>Subscribed: {item.subscription != 'default' ? 'True' : 'False'}</p>
                    <div style={this.selectStyle()} onClick={this.handleSelect} {...user}></div>
                  </div>
                )
              })
            }
          </div>
          <div className="push-wrap">
            <input name="title" placeholder="Enter title" onChange={this.handleChange}></input>
            <input name="body" placeholder="Enter body" onChange={this.handleChange}></input>
            <button onClick={this.handleClick}>Push</button>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    Users: state.users.users
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    Push: (target, title, body) => dispatch(Notifications(target, title, body)),
    Get: () => dispatch(Endpoints())
  }
}

Users = connect(
  mapStateToProps,
  mapDispatchToProps
)(Users)

export default Users
