import React, { Component } from 'react'
import { Power2, TimelineMax } from "gsap";
import { connect } from 'react-redux'

import Login from './components/login/login'

const t = new TimelineMax();

class Landing extends Component {

  componentDidMount() {
    const main = this.refs.main

    t.to(main, 2, {
      opacity: 1,
      backgroundPosition: "0% 0%",
      ease: Power2.easeInOut
    })
  }

  componentDidUpdate() {
    const main = this.refs.main
    const { Auth } = this.props

    if (Auth == true) {
      t.to(main, 2, {
        opacity: 1,
        backgroundPosition: "100% 0%",
        ease: Power2.easeInOut
      })
      .to(main, 0.5, {
        opacity: 0,
        onComplete: this.props.AuthHandler
      }, '-=1.5')
    }
  }

  render() {
    return (
      <div ref="main" className="landing-page">
        <Login />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    Auth: state.userservice.auth,
  }
}

Landing = connect(
  mapStateToProps,
  null
)(Landing)

export default Landing
