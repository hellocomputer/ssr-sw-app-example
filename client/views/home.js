import React, { Component } from 'react'

import { Power2, TimelineLite } from "gsap";

const t = new TimelineLite();

class Home extends Component {

  render() {
    const { Msg } = this.props
    return (
      <div ref="main" className="home">
        <h1>{Msg}</h1>
      </div>
    )
  }
}
export default Home
