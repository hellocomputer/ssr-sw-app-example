import React from 'react'
import { Route } from 'react-router-dom'

import Home from '../../home'
import Users from '../../users'
import Profile from '../../profile'
import Nav from './nav'

const Routes = ({ Logout, Msg }) => (
  <div className="page">
    <Nav logoutAction={Logout}/>
    <Route exact path='/' component={() => {
      return <Home Msg={Msg}/>
    }} />
    <Route exact path='/profile' component={Profile} />
    <Route exact path='/users' component={Users} />
  </div>
)

export default Routes
