import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'

import { Power2, TimelineLite } from "gsap"

import Sworker from '../sworker/sworker'

const t = new TimelineLite();

class Nav extends Component {
  constructor(props) {
    super(props)
    this.state = {
      active: false,
      throttle: false
    }
  }

  animateOpen() {
    const nav = this.refs.navMenu
    const main = this.refs.main
    const openSwi = this.refs.openSwitch
    const closeSwi = this.refs.closeSwitch
    const notif = this.refs.notif

    const links = document.getElementsByClassName('links')

    if (this.state.active) {
      t.to(main, 0.01, {
        height: "100%"
      })
      .to(main, 0.25, {
        width: "100%",
        background: "rgba(236,240,241,1)",
        ease: Power2.easeInOut
      })
      .to(nav, 0.25, {
        opacity: 1,
        display: "flex",
      })
      .to(notif, 0.25, {
        opacity: 0.75
      })
      .staggerTo(links, 0.25, {
        opacity: 1,
        onComplete: this.throttle
      }, 0.1, "-=0.25")
    }
  }

  animateClose() {
    const nav = this.refs.navMenu
    const main = this.refs.main

    if(!this.state.active) {
      t.to(main, 0.25, {
        width: "100px",
        background: "rgba(236,240,241,0)",
        ease: Power2.easeInOut
      })
      .to(main, 0.01, {
        height: "100px",
        onComplete: this.throttle
      })

    }
  }

  componentDidUpdate() {
    if (this.state.active && this.state.throttle == true) {
      return this.animateOpen()
    }

    if (!this.state.active && this.state.throttle == true) {
      return this.animateClose()
    }
  }

  activate = () => {
    this.setState({
      active: !this.state.active,
      throttle: true
    })
  }

  throttle = () => {
    this.setState({
      throttle: false
    })
  }

  componentDidMount() {
    const main = this.refs.main

    t.to(main, 0.25, {
      opacity: 1
    })
  }

  render() {
    let { throttle } = this.state
    return (
      <div ref="main" className="nav-container">
        {
          this.state.active ?
            <div ref="notif" className="notifSwitch">
              <Sworker />
            </div>
          : null
        }
        <div ref="closeSwitch" className={this.state.active ? "closeSwitch" : "openSwitch"} onClick={throttle ? null : this.activate}></div>
        {this.state.active ? <ul ref="navMenu">
          <NavLink exact to='/'><li onClick={this.activate} className="links">home</li></NavLink>
          <NavLink exact to='/profile'><li onClick={this.activate} className="links">profile</li></NavLink>
          <NavLink exact to='/users'><li onClick={this.activate} className="links">users</li></NavLink>
          <a onClick={this.props.logoutAction} href="/"><li className="links">logout</li></a>
        </ul> : null }
      </div>
    )
  }
}

export default Nav
