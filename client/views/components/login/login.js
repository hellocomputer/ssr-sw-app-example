import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Power2, TimelineLite } from "gsap";

import { userLogin, userReg, userVerify } from '../../../actions/userservice'

import Form from './form'

const t = new TimelineLite();

class Login extends Component {
  constructor(props) {
    super(props)

  }

  componentDidUpdate() {
    const { Auth, Fetch } = this.props
    const main = [this.refs.main, this.refs.p];
    const over = this.refs.over;

    if (Auth == false && Fetch == false) {
      t.to(over, 0.5, {
        opacity: 1,
        transform: "translateY(0px)"
      }, '+=1')
      .to(main, 0.5, {
        opacity: 1,
        ease: Power2.easeOut
      })
    }

    if (Auth == true) {
      t.to(main, 0.5, {
        opacity: 0,
        ease: Power2.easeInOut,
        onComplete: this.props.LoginAuthHandler
      })
    }
  }

  trueStyle() {
    return {
      display: 'none'
    }
  }

  falseStyle() {
    return {
      color: 'rgba(231,76,60,1)',
      border: '1px solid rgba(231,76,60,1)',
      padding: "5px 7.5px",
    }
  }

  render() {
    const { Auth, Name, Msg, Login, Register } = this.props
    let style = Auth ? this.trueStyle() : this.falseStyle();

    return (
        <div ref="over" className="color-overlay-secondary">
          <div ref="main" id="login" className="login">
            <p ref="p" style={Msg ? style : null}>{Msg ? Msg : 'Enter Login details below'}</p>
            <Form loginAction={Login} regAction={Register} />
          </div>
        </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    Auth: state.userservice.auth,
    Name: state.userservice.name,
    Msg: state.userservice.msg,
    Fetch: state.userservice.isFetching,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    Login: (name, pass) => dispatch(userLogin(name, pass)),
    Register: (name, pass) => dispatch(userReg(name, pass)),
  }
}

Login = connect(
  mapStateToProps,
  mapDispatchToProps
)(Login)

export default Login
