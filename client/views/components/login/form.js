import React, { Component } from 'react'
import axios from 'axios'

const t = new TimelineLite();

class Form extends Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      pass: '',
    }
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  handleSubmit = (e) => {
    e.preventDefault()
  }

  handleLogin = () => {
    const { loginAction } = this.props
    loginAction(this.state.name, this.state.pass)
  }

  handleReg = () => {
    const { regAction } = this.props
    regAction(this.state.name, this.state.pass)
  }

  componentDidMount() {
    const main = [this.refs.name, this.refs.pass, this.refs.log, this.refs.reg]

    t.staggerTo(main, 0.5, {
      opacity: 1,
      transform: "translateX(0px)"
    }, 0.05, "+=1.5")
  }

  render() {
    return (
      <form ref="main" onSubmit={this.handleSubmit}>
        <input ref="name" name='name' type='text' onChange={this.handleChange} placeholder={'Username'}></input>
        <input ref="pass" name='pass' type='password' onChange={this.handleChange} placeholder={'Password'}></input>
        <div className="btn-wrap">
          <button ref="log" className="button" onClick={this.handleLogin}>Login</button>
          <button ref="reg" className="button" onClick={this.handleReg}>Sign up</button>
        </div>
      </form>
    )
  }
}

export default Form
