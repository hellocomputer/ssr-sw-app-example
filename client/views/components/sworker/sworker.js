import React, { Component } from 'react'
import { connect } from 'react-redux'

import  { userSubscribe, userWebpush } from '../../../actions/userservice'

const applicationServerPublicKey = 'BGH80BsTKL5mYAn1Nz1MMrL9xsOvFofbKHkJkt8DLO4By2Oz4785JzaD8V8I5w4eWEbGg3vCWCPlDYhsyworDu8'
const privateKey = '5-_780shq_KiU0FkAQDJBM7QkMyee2LMNjh6adtpQEo'

class Sworker extends Component {
  constructor(props) {
    super(props)
    this.state = {
      swRegister: null,
      swSubscription : '',
      swSwitch: true
    }
  }

  updateSubOnServer = (subscription, active) => {
    const { Subscribe } = this.props

    this.setState({
      swSubscription: JSON.stringify(subscription)
    })

    Subscribe(JSON.stringify(subscription), active)
  }

  urlB64ToUint8Array = (base64String) => {
    const padding = '='.repeat((4 - base64String.length % 4) % 4)
    const base64 = (base64String + padding)
      .replace(/\-/g, '+')
      .replace(/_/g, '/')

    const rawData = window.atob(base64)
    const outputArray = new Uint8Array(rawData.length)

    for (let i = 0; i < rawData.length; ++i) {
      outputArray[i] = rawData.charCodeAt(i)
    }
    return outputArray
  }

  subscribeUser = () => {
    const { swRegister } = this.state

    const applicationServerKey = this.urlB64ToUint8Array(applicationServerPublicKey)

    swRegister.pushManager.subscribe({
      userVisibleOnly: true,
      applicationServerKey: applicationServerKey
    })
    .then(subscription => {
      this.updateSubOnServer(subscription, true)
    })
    .catch(err => {
      if (err instanceof DOMException) {
        console.log(err)
      } else {
        return
      }
    })
  }

  unsubscribeUser = () => {
    const { swRegister } = this.state

    swRegister.pushManager.getSubscription().then(subscription => {
      subscription.unsubscribe().then(result => {
        this.updateSubOnServer(null, !result)
      })
      .catch(err => {
        console.log(err)
      })
    })
  }

  init = () => {
    const { swRegister } = this.state

    navigator.serviceWorker.register("./service-worker.js")
    .then(registration => {
      this.setState({
        swRegister: registration
      })
    })
    .catch(err => {
      console.log(err)
    })
  }

  reqPush = () => {
    const { swSwitch } = this.state

    this.setState({
      swSwitch: !swSwitch
    })

    if (swSwitch == true) {
      this.subscribeUser()
    } else {
      this.unsubscribeUser()
    }
  }

  componentDidMount() {
    const { Apn } = this.props

    this.setState({
      swSwitch: !Apn
    })

    if ('serviceWorker' in navigator) {
      this.init()
    }
  }

  render() {
    const { swSwitch } = this.state
    const { Apn } = this.props
    return (
      <div className="sw">
        <p>Enable notifications</p>
        <div className="switch" onClick={this.reqPush}>
          <div className="inner-switch" style={swSwitch ? {background: "grey"} : { right: "0px", background: "#2a2c2b" }}>
            <div>{ swSwitch ? "NO" : "YES" }</div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStatetoProps = (state) => {
  return {
    Apn: state.userservice.apn
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    Webpush: (subscription) => dispatch(userWebpush(subscription)),
    Subscribe: (endpoint, active) => dispatch(userSubscribe(endpoint, active))
  }
}

Sworker = connect(
  mapStatetoProps,
  mapDispatchToProps
)(Sworker)

export default Sworker
