import React from 'react'

let x = 1

let Increase = ({ count }) => {
  
  return (
    <div className="container">
      <button onClick={() => count(x)}>+</button>
      <input onChange={(e) => x = parseInt(e.target.value)}></input>
    </div>
  )
}

export default Increase
