import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Power2, TimelineLite } from "gsap";

import { increaseCount, decreaseCount } from '../../../actions/counter'

import Increase from './increase.js'
import Decrease from './decrease.js'

const t = new TimelineLite();

class Counter extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    const main = this.refs.main;

    t.to(main, 0.5, {
      y: 0,
      opacity: 1,
      ease: Power2.easeOut
    })
  }

  render() {
    const { increase, decrease, value} = this.props
    return (
      <div ref="main" className="counter">
        <h1>{value}</h1>
        <Increase count={increase} />
        <Decrease count={decrease} />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    value: state.counter.count
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    increase: (data) =>  dispatch(increaseCount(data)),
    decrease: (data) =>  dispatch(decreaseCount(data))
  }
}

Counter = connect(
  mapStateToProps,
  mapDispatchToProps
)(Counter)

export default Counter
