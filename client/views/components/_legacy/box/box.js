import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Power2, TimelineLite } from "gsap";
import { boxes } from '../../../actions/boxes'

const t = new TimelineLite();
let boxObj

class Boxes extends Component {
  constructor(props) {
    super(props)
  }

  componentWillMount() {
    this.props.boxes()
  }

  componentDidUpdate() {
    boxObj = document.getElementsByClassName('Box');
    t.staggerTo( boxObj, 1, {
        opacity: 1,
        ease: Power2.easeOut
      }, 0.05,)
  }

  render() {
    const { Value } = this.props
    return (
      <div className={"box-container"}>
        <div ref="main" className={'Boxes'}>
            {Object.keys(Value).map(key =>
              <div className={'Box'} key={key}>
                <img src={Value[key].url}/>
              </div>
            )}
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    Value: state.boxes.data
  }
}

Boxes = connect(
  mapStateToProps,
  { boxes }
)(Boxes)

export default Boxes
