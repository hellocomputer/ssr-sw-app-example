import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Route, withRouter, Redirect } from 'react-router-dom'
import { Power2, TimelineMax } from "gsap";
import { push } from 'react-router-redux';

import { userLogout, userVerify } from './actions/userservice'

import Routes from './views/components/nav/routes'
import Landing from './views/landing'

const t = new TimelineMax();

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      AuthComplete: false,
    }
  }

  AuthHandler = () => {
    const { Auth } = this.props

    this.setState({
      AuthComplete: Auth
    })
  }

  componentDidMount() {
    const { verifyAction } = this.props
    verifyAction()
  }

  Logout = () => {
    const { LogoutAction } = this.props

    LogoutAction()

    this.setState({
      AuthComplete: false
    })
  }

  render() {
    const { AuthComplete } = this.state
    const { Msg } = this.props

    return (
      <div className="App container">
        {AuthComplete ? <Routes Logout={this.Logout} Msg={Msg}/> : <Landing AuthHandler={this.AuthHandler}/>}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    Auth: state.userservice.auth,
    Msg: state.userservice.msg
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    LogoutAction: () => dispatch(userLogout()),
    verifyAction: () => dispatch(userVerify())
  }
}

App = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(App))

export default App
