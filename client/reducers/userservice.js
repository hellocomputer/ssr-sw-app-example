import { login, register, verify, logout } from '../actions/userservice'

function userservice(state = {auth: false, msg: '', isFetching: true}, action) {
  switch (action.type) {
    case login:
      return {auth: action.isAuth, msg: action.msg, apn: action.apn}
    case register:
      return {auth: action.isAuth, msg: action.msg}
    case verify:
      return {auth: action.isAuth, msg: action.msg, isFetching: action.isFetching, apn: action.apn}
    case logout:
      return {auth: action.isAuth, msg: action.msg}
    default:
      return state
  }
}

export default userservice
