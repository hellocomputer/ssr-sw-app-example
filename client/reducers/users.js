import { get } from '../actions/users'

function users(state = {users: []}, action) {
  switch (action.type) {
    case get:
      return {users: action.users}
    default:
      return state
  }
}

export default users
