import { combineReducers } from 'redux';

import userservice from './userservice'
import users from './users'

const rootReducers = combineReducers({
  userservice,
  users
})

export default rootReducers
