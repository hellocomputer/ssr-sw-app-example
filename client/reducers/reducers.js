import { combineReducers } from 'redux'
import { inc } from '../actions/actions'

function counter(state = {count: 0}, action) {
  const count = state.count
  switch (action.type) {
    case 'increase':
      return {count: count + action.num}
    default:
      return state
  }
}

const reduc = combineReducers({
  counter
})

export default reduc
