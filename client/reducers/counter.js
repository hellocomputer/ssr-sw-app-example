import { increase, decrease } from '../actions/counter'

function counter(state = { count: 0 }, action) {
  const count = state.count
  switch (action.type) {
    case increase:
      return {count: count + action.data}
    case decrease:
      return {count: count - action.data}
    default:
      return state
  }
}

export default counter
