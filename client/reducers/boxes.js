import { createBoxes } from '../actions/boxes'

function boxes(state = {data: {}}, action) {
  const boxes = state.all
  switch (action.type) {
    case createBoxes:
      return {...state, data: action.payload}
    default:
      return state
  }
}

export default boxes
