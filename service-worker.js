const swActivate = new BroadcastChannel('client')
let link
// self.skipWaiting()
self.addEventListener('install', (event) => {
  event.waitUntil(
    caches.open('offline').then(cache => {
      return cache.addAll(
        [
          '/main-7497202ac6e492c027e6.min.js',
          '/index.html',
          '/manifest.json',
          '/assets/bg.jpg',
          '/assets/icon.jpg',
          '/service-worker.js'
        ]
      )
    })
  )
  console.log('installing...')
})

self.addEventListener('fetch', (event) => {
  event.respondWith(
    caches.match(event.request).then(function(response) {
      return response || fetch(event.request);
    })
  );
})

let x = new Promise((resolve, reject) => {

  self.addEventListener('activate', (event) => {
    event.waitUntil(self.clients.claim())
    console.log('activating...')
    resolve()
  })
})
.then(res => {
  //this can be used to trigger an event on the client side if the same
  //BroadcastChannel has been created, however this will currently cause an
  //issue on mobile so have commented out the channel creation on client
  swActivate.postMessage('activate')
})
.catch(err => {
  console.log(err)
})

self.addEventListener('push', (event) => {

  const data = JSON.parse(event.data.text())

  link = data.notification.link

  const title = data.notification.title
  const options = {
    body: data.notification.body,
    icon: data.notification.icon,
  }

  event.waitUntil(self.registration.showNotification(title, options))
})

self.addEventListener('notificationclick', (event) => {
  console.log(link)
  event.notification.close()
  event.waitUntil(
    clients.openWindow(link)
  )
})
