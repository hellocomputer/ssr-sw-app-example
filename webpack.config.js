'use strict'

const path = require('path');
const webpack = require('webpack');
const ProdFolderUpdatePlugin = require('prod-folder-update-webpack-plugin');
// const ExtractTextPlugin = require('extract-text-webpack-plugin');
// const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  devtool: 'eval-source-map',
  entry: [
    path.join(__dirname, 'client.js')
  ],
  output: {
    path: path.join(__dirname, '/dist/'),
    filename: "bundle.js",
    publicPath: '/'
  },
  plugins: [
    new ProdFolderUpdatePlugin({
      files: [
        './service-worker.js',
        './manifest.json',
        './assets',
      ],
      path: path.join(__dirname, '/dist/')
    })
  ],
  module: {
    loaders: [{
      test: /\.jsx?$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
      query: {
        "presets": ["es2015", "stage-0", "react"]
      }
    }, {
      test: /\.json?$/,
      loader: 'json'
    }, {
      test: /\.(jpg|png)$/,
      loader: 'file'
    }]
  }
}
