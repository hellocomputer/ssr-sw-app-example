import React from 'react'
import express from 'express'
import mongoose from 'mongoose'
import bodyParser from 'body-parser'
import reduxThunk from 'redux-thunk'
import session from 'express-session'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import { renderToString } from 'react-dom/server'
import {
  StaticRouter,
  browserHistory
} from 'react-router-dom'

import rootReducers from './client/reducers/_index'

import App from './client/app'

const app = express()

app.use((req, res, next) => {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
	res.setHeader('Access-Control-Allow-Credentials', true);
	if ('OPTIONS' === req.method) {
		res.send(204);
	} else {
		next();
	}
});

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: false,
  cookie: { secure: false }
}))

require('./api/routes')(app)
mongoose.connect('mongodb://Lloyd:900213@ds261527.mlab.com:61527/react-ssr-mlab')

app.set('port', (process.env.PORT || 5000))
app.use('/', express.static('dist'))
app.use(handleRender)

function handleRender(req, res) {
  const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore)
  let store = createStoreWithMiddleware(rootReducers, preloadedState)

  const context = {}

  const html = renderToString(
    <Provider store={store}>
      <StaticRouter location={req.url} context={context}>
        <App />
      </StaticRouter>
    </Provider>
  )

  const preloadedState = store.getState()

  res.send(renderFullPage(html, preloadedState))
}

function renderFullPage(html, preloadedState) {
  return `
  <!doctype html>
   <html lang="en">
     <head>
       <meta charset="utf-8">
       <meta http-equiv="x-ua-compatible" content="ie=edge">
       <title></title>
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <meta name="apple-mobile-web-app-capable" content="yes">
       <meta name="mobile-web-app-capable" content="yes">
       <link rel=stylesheet href="/_site.css">
       <link rel="manifest" href="/manifest.json">
       <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
       <script type="text/javascript" src="/service-worker.js"></script>
     </head>
     <body>
       <div id="root">${html}</div>
       <script src="/bundle.js"></script>
     </body>
   </html>
   `
}

app.get('/service-worker.js', (req, res) => {
    res.sendFile(path.resolve(__dirname, '.', 'dist', 'service-worker.js'));
});

app.listen(app.get('port'), (err) => {
  if (err) {
    console.log(err)
  }
  console.log('Listening on port', app.get('port'))
})
