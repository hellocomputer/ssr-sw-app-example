'use strict';

const fs = require('fs');
const path = require('path');

let controllers = {};
var controllerPath = path.join(__dirname, '../controllers/');

fs.readdirSync(controllerPath).forEach(function(file) {
  let name = path.basename(file, '.js');
  controllers[name] = require(path.join(controllerPath, file));
});

module.exports = controllers;
