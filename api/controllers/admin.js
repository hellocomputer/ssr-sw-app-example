'use strict;'

const User = require('../models/user.js')
const webpush = require('web-push')

const Admin = module.exports

const publicKey = 'BGH80BsTKL5mYAn1Nz1MMrL9xsOvFofbKHkJkt8DLO4By2Oz4785JzaD8V8I5w4eWEbGg3vCWCPlDYhsyworDu8'
const privateKey = '5-_780shq_KiU0FkAQDJBM7QkMyee2LMNjh6adtpQEo'

Admin.users = (req, res) => {
  User.Model.find({}, (err, data) => {
    if (err) {
      return console.log(err)
    }
    if (data) {
      return res.send({userlist: data})
    }
  })
}

Admin.push = (req, res) => {
  const params = {
    "notification": {
      "title": req.body.title,
      "body": req.body.body,
      "link": "https://react-ssr-app.herokuapp.com/",      
    }
  }

  webpush.sendNotification(JSON.parse(req.body.target), JSON.stringify(params))
  .catch(err => {
    console.log(err)
  })

  return res.sendStatus(200)
}
