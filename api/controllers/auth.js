'use strict';
const User = require('../models/user.js')
const webpush = require('web-push')
const path = require('path')

const publicKey = 'BGH80BsTKL5mYAn1Nz1MMrL9xsOvFofbKHkJkt8DLO4By2Oz4785JzaD8V8I5w4eWEbGg3vCWCPlDYhsyworDu8'
const privateKey = '5-_780shq_KiU0FkAQDJBM7QkMyee2LMNjh6adtpQEo'

const Auth = module.exports

webpush.setVapidDetails(
  'mailto:lloydleagas@gmail.com',
  publicKey,
  privateKey
)

const params = {
  "notification": {
    "title": "I am title",
    "body": "I am body",
    "link": "https://react-ssr-app.herokuapp.com"
  }
}

Auth.curltest = (req, res) => {
  console.log("route hit")
  return res.send({msg: "hello world"})
}

Auth.webpush = (req, res) => {
  User.Model.findOne({
    name: req.session.verify,
  }, (err, data) => {
    if (err) {
      console.log(err)

      return res.sendStatus(500)
    }
    if (data) {
      webpush.sendNotification(JSON.parse(data.subscription), JSON.stringify(params))
      .catch(err => {
        console.log(err)
      })

      return res.sendStatus(200)
    }
  })
}

Auth.subscribe = (req, res) => {
  User.Model.findOneAndUpdate(
    {
      name: req.session.verify
    },
    {
      subscription: req.body.subscription,
      active: req.body.active
    },
    {new: false}, (err, data) => {
      if (err) {
        console.log(err)

        return res.sendStatus(500)
      }
      if (data) {

        return res.sendStatus(200)
      }
    }
  )
}

Auth.logout = (req, res) => {
  req.session.verify = null;

  return res.send({isAuth: false, message: ''})
}

Auth.verify = (req, res) => {
  User.Model.findOne({
    name: req.session.verify,
  }, (err, data) => {
    if (req.session.verify) {

      return res.send({isAuth: true, message: 'Welcome back ' + req.session.verify, apn: data.active})
    }
    return res.send({isAuth: false, message: ''});
  })
}

Auth.login = (req, res) => {
  User.Model.findOne({
    name: req.body.username,
    pass: req.body.password,
  }, (err, data) => {
    if (data) {
      req.session.verify = data.name

      return res.send({isAuth: true, message: 'Welcome back ' + req.body.username, apn: data.active})
    }

    return res.send({isAuth: false, message: 'Incorrect login details'})
  })
}

Auth.register = (req, res) => {
  let isValid = true
  if (req.body.username.length < 3) {
    isValid = false
  }
  if (!isValid) {

    return res.send({isAuth: false, message: 'Username is too short'})
  }
  User.Model.find({
    name: req.body.username,
  }, (err, data) => {
    if (err) {

      return res.send({isAuth: false, message: 'Username Error'})
    }
    if (data.length) {

      return res.send({isAuth: false, message: 'Username is already in Use'})
    }
    User.Model.create({
      name: req.body.username,
      pass: req.body.password,
      subscription: "default"
    }, (err, user) => {
      req.session.verify = user.name

      return res.send({isAuth: true, message: 'Account created for ' + req.body.username})
    })
  })
}
