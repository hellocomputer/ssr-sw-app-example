'use strict';

const User = module.exports;

const Mongoose = require('mongoose');

const Schema = new Mongoose.Schema({
  name: { type: String },
  pass: { type: String },
  email: { type: String },
  age: { type: String },
  sex: { type: String },
  language: { type: String },
  subscription: { type: String },
  active: { type: Boolean },
}, {
  timestamps: true
});

const Model = Mongoose.model('User', Schema);

User.Model = Model;
