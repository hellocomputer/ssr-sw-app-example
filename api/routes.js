const express = require('express');
const ctrls = require('./lib/loadControllers');

module.exports = function(app) {

  const router = express.Router();

  router.get('/users', ctrls.admin.users)

  router.post('/push', ctrls.admin.push)

  router.post('/webpush', ctrls.auth.webpush)
  router.post('/subscribe', ctrls.auth.subscribe)
  router.post('/login', ctrls.auth.login)
  router.post('/register', ctrls.auth.register)
  router.post('/verify', ctrls.auth.verify)
  router.post('/logout', ctrls.auth.logout)
  router.post('/curltest', ctrls.auth.curltest)

  app.use('/api', router);
}
